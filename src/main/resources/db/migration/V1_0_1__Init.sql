create table empresa
(
    empresa_id     bigint auto_increment
        primary key,
    activo         bit          null,
    direccion      varchar(255) null,
    nombre_empresa varchar(255) null,
    telefono       bigint       null
);

