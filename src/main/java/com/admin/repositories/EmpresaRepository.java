package com.admin.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.admin.entities.Empresa;

@Repository
public interface EmpresaRepository extends PagingAndSortingRepository<Empresa, Long> {

   //@formatter:off
   @Query("SELECT e FROM Empresa e"
         + " where :direccion is null or e.direccion = :direccion"
         + " and :nombreEmpresa is null or e.nombreEmpresa = :nombreEmpresa"
         + " and :telefono is null or  e.telefono = :telefono")
   //@formatter:on
   Page<Empresa> findByParams(@Param("direccion") String direccion, @Param("nombreEmpresa") String nombreEmpresa, @Param("telefono") Long telefono,
         Pageable page);

}
