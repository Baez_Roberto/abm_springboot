package com.admin.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.admin.dtos.REmpresa;
import com.admin.entities.Empresa;
import com.admin.mappers.EmpresaMapper;
import com.admin.repositories.EmpresaRepository;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@AllArgsConstructor
public class EmpresaService {

   private final EmpresaRepository repository;

   private final EmpresaMapper mapper = EmpresaMapper.INSTANCE_EMPRESA;

   public REmpresa create(REmpresa dto) {
      Empresa empresa = mapper.fromDto(dto);
      return mapper.toDto(repository.save(empresa));
   }

   public REmpresa update(REmpresa dto) {
      Optional<Empresa> empresaOptional = repository.findById(dto.getEmpresaId());
      Empresa empresa = empresaOptional.get();
      empresa = mapper.mergeEntity(dto, empresa);

      return mapper.toDto(repository.save(empresa));
   }

   public REmpresa findById(Long empresaId) {
      Optional<Empresa> empresaOptional = repository.findById(empresaId);
      return mapper.toDto(empresaOptional.get());
   }

   public REmpresa deleteById(Long empresaId) {
      Optional<Empresa> empresaOptional = repository.findById(empresaId);
      repository.deleteById(empresaId);
      return mapper.toDto(empresaOptional.get());
   }

   public Page<REmpresa> queryByParams(String direccion, String nombreEmpresa, Long telefono, Pageable page) {
      return mapper.toPage(repository.findByParams(direccion, nombreEmpresa, telefono, page));
   }

}
