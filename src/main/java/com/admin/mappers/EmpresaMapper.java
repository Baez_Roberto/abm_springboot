package com.admin.mappers;

import java.util.List;
import java.util.stream.Collectors;

import static org.mapstruct.factory.Mappers.getMapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.springframework.data.domain.Page;

import com.admin.dtos.REmpresa;
import com.admin.entities.Empresa;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface EmpresaMapper {

   EmpresaMapper INSTANCE_EMPRESA = getMapper(EmpresaMapper.class);

   Empresa fromDto(REmpresa dto);

   REmpresa toDto(Empresa entity);

   default Page<REmpresa> toPage(Page<Empresa> page) {
      return page.map(this::toDto);
   }

   default List<REmpresa> toList(List<Empresa> list) {
      return list.stream().map(this::toDto).collect(Collectors.toList());
   }

   default List<Empresa> toEntityList(List<REmpresa> list) {
      return list.stream().map(this::fromDto).collect(Collectors.toList());
   }

   //@formatter:off
   @Mappings({
         @Mapping(target = "empresaId", ignore = true) })
   //@formatter:on
   Empresa mergeEntity(REmpresa dto, @MappingTarget Empresa entity);

}
