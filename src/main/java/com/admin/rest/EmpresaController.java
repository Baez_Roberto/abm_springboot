package com.admin.rest;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import javax.validation.Valid;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.admin.dtos.REmpresa;
import com.admin.services.EmpresaService;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Validated
@RestController
@AllArgsConstructor
@RequestMapping("/empresa")
public class EmpresaController {

   private final EmpresaService service;

   @PostMapping
   @ApiOperation(value = "Create  Empresa.", notes = "Endpoint to create a  Empresa entity.", produces = APPLICATION_JSON_VALUE, consumes =
         APPLICATION_JSON_VALUE)
   @ApiResponses({ @ApiResponse(code = 200, message = "Created successfully"), @ApiResponse(code = 412, message = "Parameters validator errors"),
         @ApiResponse(code = 500, message = "Internal Error, support is required"), @ApiResponse(code = 401, message = "Unauthorized"),
         @ApiResponse(code = 403, message = "Forbidden") })
   public REmpresa create(@RequestBody @Valid REmpresa dto) {
      return service.create(dto);
   }

   @PutMapping
   @ApiOperation(value = "Update  Empresa.", notes = "Endpoint to update a  Empresa entity.", produces = APPLICATION_JSON_VALUE, consumes =
         APPLICATION_JSON_VALUE)
   @ApiResponses({ @ApiResponse(code = 200, message = "Updated successfully"), @ApiResponse(code = 412, message = "Parameters validator errors"),
         @ApiResponse(code = 500, message = "Internal Error, support is required"), @ApiResponse(code = 401, message = "Unauthorized"),
         @ApiResponse(code = 403, message = "Forbidden") })
   public REmpresa update(@RequestBody @Valid REmpresa dto) {
      return service.update(dto);
   }

   @GetMapping
   @ApiOperation(value = "Find  Empresa by id.", notes = "Endpoint to find  Empresa entity by id.", produces = APPLICATION_JSON_VALUE, consumes =
         APPLICATION_JSON_VALUE)
   @ApiResponses({ @ApiResponse(code = 200, message = "Recurrent service found"), @ApiResponse(code = 412, message = "Parameters validator errors"),
         @ApiResponse(code = 500, message = "Internal Error, support is required"), @ApiResponse(code = 401, message = "Unauthorized"),
         @ApiResponse(code = 403, message = "Forbidden") })
   public REmpresa findById(@RequestParam(value = "empresaId") Long empresaId) {
      return service.findById(empresaId);
   }

   @DeleteMapping
   @ApiOperation(value = "Delete  Empresa by id.", notes = "Endpoint to delete  Empresa entity by id.", produces = "application"
         + "/json", consumes = "application/json")
   public REmpresa deleteById(@RequestParam(value = "empresaId") Long empresaId) {
      return service.deleteById(empresaId);
   }

   @GetMapping("/query")
   @ApiOperation(value = "List  Empresa Data by params.", notes = "Endpoint to find  Empresa Data by params.", produces = APPLICATION_JSON_VALUE,
         consumes = APPLICATION_JSON_VALUE)
   @ApiResponses({ @ApiResponse(code = 200, message = " Empresa Data found"), @ApiResponse(code = 412, message = "Parameters validator errors"),
         @ApiResponse(code = 500, message = "Internal Error, support is required"), @ApiResponse(code = 401, message = "Unauthorized"),
         @ApiResponse(code = 403, message = "Forbidden") })
   public Page<REmpresa> queryByParams(@RequestParam(value = "direccion", required = false) String direccion,
         @RequestParam(value = "nombreEmpresa", required = false) String nombreEmpresa,
         @RequestParam(value = "telefono", required = false) Long telefono, Pageable page) {
      return service.queryByParams(direccion, nombreEmpresa, telefono, page);
   }

}

