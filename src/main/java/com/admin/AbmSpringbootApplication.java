package com.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@EnableAsync
@EnableCaching
@EnableJpaAuditing
@SpringBootApplication
@AllArgsConstructor
public class AbmSpringbootApplication {

   public static void main(String[] args) {
      SpringApplication.run(AbmSpringbootApplication.class, args);
   }

}
