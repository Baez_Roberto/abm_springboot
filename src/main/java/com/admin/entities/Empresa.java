package com.admin.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "empresa")
public class Empresa {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long empresaId;

   @Column
   private String nombreEmpresa;

   @Column
   private String direccion;

   @Column
   private Long telefono;

   @Column
   @Builder.Default
   private Boolean activo = Boolean.TRUE;

}
